package com.wakeup.app;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.wakeup.app.communication.PingProgressCallback;
import com.wakeup.app.model.NetworkDevice;
import com.wakeup.app.model.NetworkDeviceRepository;

/**
 * A view to show a network device.
 */
public class FragmentDeviceView extends Fragment implements PingProgressCallback {

    private final NetworkDeviceControllerAction callback;
    private final int position;
    private final NetworkDeviceRepository repository;

    public FragmentDeviceView(NetworkDeviceControllerAction callback, NetworkDeviceRepository repository, int position) {
        this.callback = callback;
        this.position = position;
        this.repository = repository;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Deserialise view
        View rootView = inflater.inflate(R.layout.fragment_device_view, container, false);

        // Set control values to network device values

        TextView nameText = (TextView) rootView.findViewById(R.id.nameText);
        TextView ipAddressText = (TextView) rootView.findViewById(R.id.ipAddressText);
        TextView portText = (TextView) rootView.findViewById(R.id.portText);
        TextView subnetMaskText = (TextView) rootView.findViewById(R.id.subnetMaskText);
        TextView macText = (TextView) rootView.findViewById(R.id.macText);

        NetworkDevice device = repository.get(position);
        nameText.setText(device.getName());
        ipAddressText.setText(device.getIpAddress());
        portText.setText(Integer.toString(device.getPortNumber()));
        subnetMaskText.setText(device.getSubnetMask());
        macText.setText(device.getMacAddress());


        // Attach button onclick handlers

        Button buttonEdit = (Button) rootView.findViewById(R.id.editDeviceButton);
        buttonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentDeviceView.this.callback.startEditNetworkDevice(FragmentDeviceView.this.position);
            }
        });
        Button buttonSend = (Button) rootView.findViewById(R.id.sendPacketButton);
        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentDeviceView.this.callback.sendNetworkDeviceWakeOnLanPacket(FragmentDeviceView.this.position);
            }
        });
        Button buttonDelete = (Button) rootView.findViewById(R.id.deleteDeviceButton);
        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentDeviceView.this.callback.removeNetworkDevice(FragmentDeviceView.this.position);
            }
        });

        // setup our ping progress callback
        final PingProgressCallback progressCallback = this;
        Button buttonSendPing = (Button) rootView.findViewById(R.id.sendPingButton);
        buttonSendPing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentDeviceView.this.callback.sendNetworkDevicePing(FragmentDeviceView.this.position, progressCallback);
            }
        });


        return rootView;
    }

    // Progress handling for ping status
    @Override
    public void OnPingProgress(String details) {
        TextView pingProgressText = (TextView) getView().findViewById(R.id.pingProgressText);
        if (pingProgressText != null)
        {
            pingProgressText.setText(details);
        }
    }

    // Ensure any ping we started is stopped.
    @Override
    public void onPause() {
        super.onPause();
        this.callback.stopNetworkDevicePing();
    }
}

