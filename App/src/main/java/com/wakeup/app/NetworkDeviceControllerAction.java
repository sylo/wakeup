package com.wakeup.app;

import com.wakeup.app.communication.PingProgressCallback;
import com.wakeup.app.model.NetworkDevice;

/**
 * Facilitate communication between fragments and main activity.
 */
public interface NetworkDeviceControllerAction {

    void startAddNetworkDevice();
    void startViewNetworkDevice(int position);
    void startEditNetworkDevice(int position);

    void addNetworkDevice(NetworkDevice device);
    void updateNetworkDevice(int position, NetworkDevice device);
    void removeNetworkDevice(int position);
    void sendNetworkDeviceWakeOnLanPacket(int position);
    void sendNetworkDevicePing(int position, PingProgressCallback callback);
    void stopNetworkDevicePing();
}
