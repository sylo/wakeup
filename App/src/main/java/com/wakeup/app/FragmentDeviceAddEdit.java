package com.wakeup.app;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.wakeup.app.model.NetworkDevice;
import com.wakeup.app.model.NetworkDeviceRepository;
import com.wakeup.app.utility.IntUtility;

/**
 * A placeholder fragment containing a simple view.
 */
public class FragmentDeviceAddEdit extends Fragment {

    private NetworkDeviceControllerAction actionCallBack;
    private View rootView;
    private NetworkDeviceRepository deviceRepository;

    // Negative value indicates we are in edit mode, otherwise add mode
    private final int position;

    // Constructor for Add
    public FragmentDeviceAddEdit(NetworkDeviceControllerAction actionCallBack, NetworkDeviceRepository deviceRepository) {
        this.actionCallBack = actionCallBack;
        this.deviceRepository = deviceRepository;
        this.position = -1;
    }

    // Constructor for Edit
    public FragmentDeviceAddEdit(NetworkDeviceControllerAction actionCallBack, NetworkDeviceRepository deviceRepository, int position) {
        this.actionCallBack = actionCallBack;
        this.deviceRepository = deviceRepository;
        this.position = position;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_device_add_edit, container, false);

        // Populate edit values
        if (IsEditMode())
        {
            NetworkDevice device = this.deviceRepository.get(this.position);
            SetViewValues(device);
        }

        // Attach add / edit onclick handler
        Button button = (Button) rootView.findViewById(R.id.addUpdateButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NetworkDevice device = GetValuesFromView();
                if (device != null)
                {
                    if (FragmentDeviceAddEdit.this.IsAddMode())
                        actionCallBack.addNetworkDevice(device);
                    else if (FragmentDeviceAddEdit.this.IsEditMode())
                        actionCallBack.updateNetworkDevice(position, device);
                }
            }
        });


        return rootView;
    }

    private boolean IsEditMode()
    {
        return this.position >= 0;
    }

    private boolean IsAddMode()
    {
        return this.position < 0;
    }

    /**
     * Creates a NetworkDevice object if the control values pass validation.
     * Otherwise null is returned.
     */
    public NetworkDevice GetValuesFromView()
    {
        // Create a network device to pass back to caller
        NetworkDevice networkDevice = new NetworkDevice();

        boolean error = false;

        // Get values from view
        EditText nameText = (EditText) rootView.findViewById(R.id.nameText);
        EditText ipAddressText = (EditText) rootView.findViewById(R.id.ipAddressText);
        EditText portText = (EditText) rootView.findViewById(R.id.portText);
        EditText subnetMaskText = (EditText) rootView.findViewById(R.id.subnetMaskText);
        EditText macText = (EditText) rootView.findViewById(R.id.macText);

        // Validate name
        {
            String name = nameText.getText().toString();
            if (name.isEmpty())
            {
                nameText.setError("Enter a name");
                error = true;
            }
            else
            {
                networkDevice.setName(name);
            }
        }

        // Validate ip address
        {
            String ipAddress = ipAddressText.getText().toString();
            if (ipAddress.isEmpty())
            {
                ipAddressText.setError("Enter IP Address");
                error = true;
            }
            else
            {
                networkDevice.setIpAddress(ipAddress);
            }
        }

        // Validate port number
        try
        {
            String port = portText.getText().toString();
            if (port.isEmpty())
                throw new NumberFormatException();

            int portNumber = Integer.parseInt(port);
            if (portNumber < 0 || portNumber > 65535)
                throw new NumberFormatException();

            networkDevice.setPortNumber(portNumber);
        }
        catch (NumberFormatException ex)
        {
            portText.setError("Enter a number between 0 and 65535");
            error = true;
        }

        // Validate subnet mask
        {
            String subnetMask = subnetMaskText.getText().toString();
            if (subnetMask.isEmpty())
            {
                networkDevice.setSubnetMask(subnetMask);
            }
            else
            {
                boolean isMaskOK = true;

                int countOfPeriods = subnetMask.length() - subnetMask.replace(".", "").length();
                String[] parts = subnetMask.split("\\.");
                if (parts.length == 4)
                {
                    for(String text : parts)
                    {
                        if (!IntUtility.isInteger(text))
                        {
                            isMaskOK=false;
                            error = true;
                            break;
                        }
                    }
                }
                else
                {
                    isMaskOK = false;
                    error = true;
                }

                if (isMaskOK)
                    networkDevice.setSubnetMask(subnetMask);
                else
                    subnetMaskText.setError("Leave blank or enter a valid subnet mask");
            }
        }

        // Validate MAC address
        {
            String macAddress = macText.getText().toString();
            if (macAddress.isEmpty())
            {
                macText.setError("Enter MAC address");
                error = true;
            }
            else
            {
                networkDevice.setMacAddress(macAddress);
            }
        }

        if (error)
            return null;
        else
            return networkDevice;
    }

    // Populates the control values with the device values
    private void SetViewValues(NetworkDevice device) {
        EditText nameText = (EditText) rootView.findViewById(R.id.nameText);
        EditText ipAddressText = (EditText) rootView.findViewById(R.id.ipAddressText);
        EditText portText = (EditText) rootView.findViewById(R.id.portText);
        EditText subnetMaskText = (EditText) rootView.findViewById(R.id.subnetMaskText);
        EditText macText = (EditText) rootView.findViewById(R.id.macText);

        nameText.setText(device.getName());
        ipAddressText.setText(device.getIpAddress());
        portText.setText(Integer.toString(device.getPortNumber()));
        subnetMaskText.setText(device.getSubnetMask());
        macText.setText(device.getMacAddress());
    }
}
