package com.wakeup.app.model;

import android.content.Context;
import android.util.Xml;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlSerializer;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Loads and saves a collection of Network Device to an XML file.
 */
public class NetworkDeviceRepository {

    // Required for file API
    private final Context context;

    // Collection of NetworkDevice
    private ArrayList<NetworkDevice> networkDevices = new ArrayList<NetworkDevice>();

    public NetworkDeviceRepository(Context context)
    {
        this.context = context;
    }

    public void Load()
    {
        // Clear the collection
        this.networkDevices.clear();

        // Open file and read to a byte array
        FileInputStream inputStream = null;
        String data = null;
        try
        {
            inputStream = this.context.openFileInput("config.txt");
            InputStreamReader streamReader = new InputStreamReader(inputStream);
            char[] buffer = new char[inputStream.available()];
            streamReader.read(buffer);
            data = new String(buffer);
            streamReader.close();
            inputStream.close();
        }
        catch (FileNotFoundException ex)
        {
            // has not run before, ignore
        }
        catch (IOException ex)
        {
            // TODO - handle problem reading file
        }

        try
        {
            if (inputStream != null)
                inputStream.close();
        }
        catch (Exception ex)
        {
            // ignore any problems with closing the input stream
        }

        if (data != null && !data.isEmpty())
        {
            // Parse the UTF-8 XML data
            try
            {
                InputStream stream = new ByteArrayInputStream(data.getBytes("UTF-8"));
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = factory.newDocumentBuilder();
                Document document = builder.parse(stream);

                // Normalise text nodes - i.e. no adjacent nor empty text nodes - they are merged
                document.getDocumentElement().normalize();

                NodeList deviceList = document.getElementsByTagName("device");
                for(int i=0; i < deviceList.getLength();i++)
                {
                    Node deviceNode = deviceList.item(i);
                    NetworkDevice device = readNetworkDevice(deviceNode);
                    networkDevices.add(device);
                }
            }
            catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            }
        }
    }

    private NetworkDevice readNetworkDevice(Node deviceNode) {

        NetworkDevice device = new NetworkDevice();
        NodeList nodes = deviceNode.getChildNodes();
        for(int j=0; j<nodes.getLength();j++)
        {
            Node value = nodes.item(j);
            //if (value.getNodeType() == Node.ELEMENT_NODE)
            String name = value.getNodeName();
            if (name.equals("name"))
            {
                device.setName(value.getTextContent());
            }
            else if (name.equals("ipaddress"))
            {
                device.setIpAddress(value.getTextContent());
            }
            else if (name.equals("port"))
            {
                device.setPortNumber(Integer.parseInt(value.getTextContent()));
            }
            else if (name.equals("mac"))
            {
                device.setMacAddress(value.getTextContent());
            }
            else if (name.equals("subnetmask"))
            {
                device.setSubnetMask(value.getTextContent());
            }
            else
            {
                // unknown
            }
        }
        return device;
    }


    /**
     * Saves data to an XML data file
     */
    public void Save() {

        // store values for next time onload
        FileOutputStream outputStream = null;

        try
        {
            outputStream = this.context.openFileOutput("config.txt", Context.MODE_PRIVATE);
            android.util.Log.v("MainActivity", this.context.getFileStreamPath("config.txt").getAbsolutePath());

            XmlSerializer serializer = Xml.newSerializer();
            serializer.setOutput(outputStream, "UTF-8");
            serializer.startDocument(null,Boolean.TRUE);
            serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);

            serializer.startTag(null,"configuration");

            for (NetworkDevice device : this.networkDevices) {

                WriteNetworkDeviceXml(serializer, device);
            }

            serializer.endTag(null,"configuration");
            serializer.endDocument();

            serializer.flush();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        try{
            if (outputStream != null)
                outputStream.close();
        }
        catch (Exception ex)
        {
            // file error
        }
    }

    private void WriteNetworkDeviceXml(XmlSerializer serializer, NetworkDevice device) throws IOException {
        serializer.startTag(null,"device");

        serializer.startTag(null,"name");
        serializer.text(device.getName());
        serializer.endTag(null, "name");

        serializer.startTag(null,"ipaddress");
        serializer.text(device.getIpAddress());
        serializer.endTag(null, "ipaddress");

        serializer.startTag(null,"port");
        serializer.text(Integer.toString(device.getPortNumber()));
        serializer.endTag(null,"port");

        serializer.startTag(null,"subnetmask");
        serializer.text(device.getSubnetMask());
        serializer.endTag(null,"subnetmask");

        serializer.startTag(null,"mac");
        serializer.text(device.getMacAddress());
        serializer.endTag(null,"mac");

        serializer.endTag(null, "device");
    }

    public void add(NetworkDevice device) {
        this.networkDevices.add(device);
        Save();
    }

    public void remove(int position) {
        this.networkDevices.remove(position);
        Save();
    }

    public void update(int position, NetworkDevice updated) {
        NetworkDevice existingDevice = this.networkDevices.get(position);
        existingDevice.set(updated);
        Save();
    }

    public ArrayList<NetworkDevice> getList() {

        return this.networkDevices;
    }

    // Returns a copy of the actual device
    public NetworkDevice get(int position) {
        return (NetworkDevice) this.networkDevices.get(position).clone();
    }
}
