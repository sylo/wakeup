package com.wakeup.app.model;

/**
 * Definition of a Network Device, in so far as useful to send it a Wake On LAN packet.
 */
public class NetworkDevice {
    private String name;
    private String ipAddress;
    private String macAddress;
    private int portNumber;
    private String subnetMask;

    public NetworkDevice()
    {
        this.name = "";
        this.ipAddress = "";
        this.macAddress = "";
        this.portNumber = 0;
        this.subnetMask = "";
    }

    public String getIpAddress(){
        return this.ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
        if(this.ipAddress == null)
            this.ipAddress = "";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        if(this.name==null)
            this.name = "";
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
        if(this.macAddress == null)
            this.macAddress = "";
    }

    public int getPortNumber() {
        return portNumber;
    }

    public void setPortNumber(int portNumber) {
        this.portNumber = portNumber;
    }


    public void set(NetworkDevice updated) {
        this.name = updated.name;
        this.ipAddress = updated.ipAddress;
        this.macAddress = updated.macAddress;
        this.portNumber = updated.portNumber;
        this.subnetMask = updated.subnetMask;
    }

    public void setSubnetMask(String subnetMask)
    {
        this.subnetMask = subnetMask;
        if (this.subnetMask == null)
            this.subnetMask = "";
    }

    public String getSubnetMask()
    {
        return this.subnetMask;
    }

    @Override
    protected Object clone()  {
        NetworkDevice d = new NetworkDevice();
        d.set(this);
        return d;
    }
}
