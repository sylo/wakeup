package com.wakeup.app;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.wakeup.app.model.NetworkDevice;
import com.wakeup.app.model.NetworkDeviceRepository;

/*
 *   Implementation of a ListView as a fragment.
 *   OnClickListener is necessary for handling onclick events which would otherwise
 *   go to the activity.
 */
public class FragmentDeviceList extends Fragment {

    private final int MENU_GROUP = 1;
    private final int MENU_ITEM_DELETE = 2;

    private FragmentDeviceListAdaptor dataAdapter;
    NetworkDeviceRepository repository;
    NetworkDeviceControllerAction actionCallback;

    public FragmentDeviceList(NetworkDeviceControllerAction actionCallback, NetworkDeviceRepository repository) {
        this.actionCallback = actionCallback;
        this.repository = repository;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.fragment_device_list, container, false);

        // Set the custom Network Device adapter for the list layout
        ListView lv = (ListView) rootView.findViewById(R.id.listView);
        dataAdapter = new FragmentDeviceListAdaptor(actionCallback, rootView.getContext(), repository);
        lv.setAdapter(dataAdapter);

        // React to user clicks on item
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parentAdapter, View view, int position, long id) {

                NetworkDevice item = dataAdapter.getItem(position);
                Toast.makeText(rootView.getContext(), "Item with id [" + id + "] - Position [" + position + "] - Item [" + item.getName() + "]", Toast.LENGTH_SHORT).show();
            }
        });

        // Register the context menu for the list view
        registerForContextMenu(lv);

        // Set up click handling on this fragment (otherwise the layout fragment click handlers get routed to the activity)
        Button b = (Button) rootView.findViewById(R.id.addBtn);
        b.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                actionCallback.startAddNetworkDevice();
            }
        });


        return rootView;
    }

    // Create a context Menu when the user long clicks on an item
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;

        NetworkDevice item = dataAdapter.getItem(info.position);
        menu.setHeaderTitle("Options for " + item.getName());
        menu.add(MENU_GROUP, 1, 1, "Details");
        menu.add(MENU_GROUP,MENU_ITEM_DELETE,2,"Delete");
    }

    // Called when list view context menu is selected
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if(item.getGroupId() == MENU_GROUP)
        {
            if(item.getItemId() == MENU_ITEM_DELETE)
            {
                AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

                Toast.makeText(getActivity(), item.getTitle() + " " + info.position, Toast.LENGTH_SHORT).show();

                actionCallback.removeNetworkDevice(info.position);

                dataAdapter.notifyDataSetChanged();
            }
        }

        return true;
    }




}