package com.wakeup.app;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.wakeup.app.model.NetworkDevice;
import com.wakeup.app.model.NetworkDeviceRepository;

/**
 * Maps the layout and view for a Planet within a list view
 */
public class FragmentDeviceListAdaptor extends ArrayAdapter<NetworkDevice> {

    private NetworkDeviceControllerAction actionCallback;
    private final Context context;
    private final NetworkDeviceRepository repository;

    public FragmentDeviceListAdaptor(NetworkDeviceControllerAction actionCallback, Context context, NetworkDeviceRepository repository) {

        super(context, R.layout.device_row, repository.getList());

        this.actionCallback = actionCallback;
        this.context = context;
        this.repository = repository;
    }

    public View getView(final int position, View convertView, ViewGroup parent)
    {
        // Load the view layout for the planet row
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.device_row, parent, false);

        // Populate the row's view with the model data
        TextView textViewName =  (TextView) rowView.findViewById(R.id.deviceName);
        TextView textViewDetails = (TextView) rowView.findViewById(R.id.deviceDetails);

        NetworkDevice device = repository.get(position);

        textViewName.setText(device.getName());
        String diameterText = String.format("%s %d %s", device.getIpAddress(), device.getPortNumber(), device.getMacAddress());
        textViewDetails.setText(diameterText);

        // Attach button click event
        Button button = (Button) rowView.findViewById(R.id.showDetailViewBtn);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FragmentDeviceListAdaptor.this.actionCallback.startViewNetworkDevice(position);
            }
        });

        // Pass back the new view for the row
        return rowView;
    }
}
