package com.wakeup.app.communication;

// Provides status on ping events
public interface PingProgressCallback {
    void OnPingProgress(String details);
}
