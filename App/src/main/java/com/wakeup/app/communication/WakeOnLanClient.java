package com.wakeup.app.communication;

import android.util.Log;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.util.Arrays;

/**
 * Sends Wake on LAN packet to a particular network address.
 */
public class WakeOnLanClient
{
    // Sends WOL direct to an IP address
    // returns string IP address where the packet was sent
    public String send(String hostName, int port, byte[] mac) throws Exception
    {
        // This subnet mask is inverted and ORed with IP address, so it is effectively ignored
        byte[] subnetMask = new byte[]{(byte)0xff, (byte)0xff, (byte) 0xff, (byte) 0xff};
        return send(hostName, port, mac, subnetMask);
    }

    // Sends WOL to a subnet
    // returns string IP address where the packet was sent
    public String send(String hostName, int port, byte[] mac, byte[] subnetMask) throws IllegalArgumentException, Exception
    {
        if (mac == null || mac.length != 6)
        {
            throw new IllegalArgumentException("mac array must be set and 6 bytes long");
        }

        if (subnetMask == null || subnetMask.length != 4)
        {
            throw new IllegalArgumentException("subnetMask must be set and 4 bytes long");
        }

        try
        {
            byte[] wakeOnLanPacket = createMagicPacket(mac);

            // Convert IP address or look up name
            InetAddress host = InetAddress.getByName(hostName);
            byte[] ipAddress = host.getAddress();

            // Invert the subnet mask
            for(int i =0; i < subnetMask.length; i++)
                subnetMask[i] = (byte) ~subnetMask[i];

            // binary-OR the IP address and the subnet mask
            for(int i =0; i < ipAddress.length; i++)
                ipAddress[i] = (byte) (ipAddress[i] | subnetMask[i]);

            // Create new host for sending
            InetAddress host2 = InetAddress.getByAddress(ipAddress);

            // Send the UDP data
            DatagramSocket socket = new DatagramSocket(null);
            DatagramPacket packet = new DatagramPacket (wakeOnLanPacket, wakeOnLanPacket.length, host2, port);
            socket.send (packet);
            socket.close ();

            Log.v("COMMS", String.format("sent to %s", host2.getHostAddress()));

            return host2.getHostAddress();
        }
        catch(SocketException se)
        {
            se.printStackTrace();
            throw new Exception("Failed to send: " + se.getMessage());
        }
        catch(Exception e)
        {
            e.printStackTrace();
            throw new Exception("Failed to send: " + e.getMessage());
        }
    }

    //
    // Creates a Wake On Lan data packet, 102 bytes long.
    //
    private byte[] createMagicPacket(byte[] mac)
    {
        byte wakeOnLanPacket[] = new byte[102];

        // Write magic header
        Arrays.fill(wakeOnLanPacket, 0, 6, (byte) 0xff);

        // Write payload. 16 x MAC address
        int bytePosition = 6;
        for(int i = 1; i <= 16; i++)
        {
            System.arraycopy(mac, 0, wakeOnLanPacket, bytePosition, 6);
            bytePosition+=6;
        }
        return wakeOnLanPacket;
    }
}
