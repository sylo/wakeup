package com.wakeup.app;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.wakeup.app.communication.PingProgressCallback;
import com.wakeup.app.communication.WakeOnLanClient;
import com.wakeup.app.model.NetworkDevice;
import com.wakeup.app.model.NetworkDeviceRepository;
import com.wakeup.app.utility.AlertMessage;
import com.wakeup.app.utility.MacConverter;
import com.wakeup.app.communication.PingTask;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class MainActivity extends ActionBarActivity implements NetworkDeviceControllerAction {

    NetworkDeviceRepository deviceRepository;
    // An async task that is used to ping a device
    PingTask pingTask = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        android.util.Log.v("MainActivity", "ON CREATE");

        setContentView(R.layout.activity_main);

        this.deviceRepository = new NetworkDeviceRepository(this);
        this.deviceRepository.Load();

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new FragmentDeviceList(this, deviceRepository))
                    .commit();
        }
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        android.util.Log.v("MainActivity", "ON START");
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        android.util.Log.v("MainActivity", "ON PAUSE");
        StopPingTask();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    // Add Network Device action handler
    @Override
    public void startAddNetworkDevice() {

        // Start a new fragment and allow user to go back
        FragmentDeviceAddEdit fragment = new FragmentDeviceAddEdit(this, this.deviceRepository);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .addToBackStack(null)
                .commit();
    }


    @Override
    public void startViewNetworkDevice(int position) {

        // Start a new fragment and allow user to go back

        NetworkDevice device = deviceRepository.get(position);

        FragmentDeviceView fragment = new FragmentDeviceView(this, this.deviceRepository, position);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .addToBackStack(null)
                .commit();
    }

    // Edit Network Device
    // Show the edit fragment. Can only have come from the view fragment.
    //
    @Override
    public void startEditNetworkDevice(int position) {
        FragmentDeviceAddEdit fragment = new FragmentDeviceAddEdit(this, deviceRepository, position);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void addNetworkDevice(NetworkDevice device) {

        // store new network device
        deviceRepository.add(device);

        // Return to previous fragment
        getSupportFragmentManager().popBackStack();

    }

    @Override
    public void updateNetworkDevice(int position, NetworkDevice device) {
        deviceRepository.update(position, device);
        getSupportFragmentManager().popBackStack();
    }

    @Override
    public void removeNetworkDevice(final int position) {
        NetworkDevice device = deviceRepository.get(position);

        DialogInterface.OnClickListener onClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (i)
                {
                    case DialogInterface.BUTTON_POSITIVE:
                        deviceRepository.remove(position);
                        getSupportFragmentManager().popBackStack();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Delete " + device.getName());
        builder.setMessage("Are you sure?")
                .setPositiveButton("Yes", onClickListener)
                .setNegativeButton("No", onClickListener)
                .show();

    }


    @Override
    public void sendNetworkDeviceWakeOnLanPacket(int position) {

        NetworkDevice device = deviceRepository.get(position);

        // Send Wake On Lan packet
        byte[] mac = MacConverter.Convert(device.getMacAddress());
        String actualIpAddress = "";
        WakeOnLanClient client = new WakeOnLanClient();
        try
        {
            String maskText = device.getSubnetMask();
            if (maskText == null || maskText.length() == 0)
            {
                // Send direct
                actualIpAddress = client.send(device.getIpAddress(), device.getPortNumber(), mac);
            }
            else
            {
                // Send to subnet
                InetAddress mask = InetAddress.getByName(device.getSubnetMask());
                byte[] byteMask = mask.getAddress();

                actualIpAddress = client.send(device.getIpAddress(), device.getPortNumber(), mac, byteMask);
            }
            Toast toast = Toast.makeText(this.getBaseContext(), String.format("Message sent to %s (%s)", device.getName(), actualIpAddress), Toast.LENGTH_SHORT);
            toast.show();
        }
        catch (Exception ex)
        {
            AlertMessage.show(this, "Error", ex.getMessage());
        }
    }

    @Override
    public void sendNetworkDevicePing(int position, PingProgressCallback callback)
    {
        NetworkDevice device = deviceRepository.get(position);

        // Note: InetAddress.isReachable according to reports does not work consistently
        // and there is some confusion as to whether it requires Root access.
        // With my tests on LAN / WAN, it simply does not work.


        // Alternative method - use system ping
        final Context context = getBaseContext();
        StopPingTask();

        pingTask = new PingTask(callback);
        pingTask.execute(device.getIpAddress());
    }

    @Override
    public void stopNetworkDevicePing() {
        StopPingTask();
    }
    // This is here so that when user exit's fragment, we ensure the ping async task is stopped.
    @Override
    public void onBackPressed() {

        StopPingTask();
        super.onBackPressed();
    }

    private void StopPingTask() {
        if (pingTask != null)
        {
            pingTask.stop();
            pingTask = null;
        }
    }


}
