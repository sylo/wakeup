package com.wakeup.app.utility;

import android.app.AlertDialog;
import android.view.Gravity;
import android.widget.TextView;

/**
 * Helper class to show dialog boxes.
 */
public class AlertMessage
{
    static public void show(android.content.Context context, String title, String message)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("OK", null);
        AlertDialog dialog = builder.show();

        // Must call show() prior to fetching text view
        TextView messageView = (TextView)dialog.findViewById(android.R.id.message);
        messageView.setGravity(Gravity.CENTER);
    }
}
