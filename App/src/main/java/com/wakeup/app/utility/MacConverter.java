package com.wakeup.app.utility;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Locale;

/**
 * Simple conversion class to parse and convert MAC string to a byte array.
 */
public class MacConverter {
    /**
     * Converts mac address text to a byte array
     * @param macAddressText
     * @return byte[6]
     */
    public static byte[] Convert(String macAddressText)
    {
        final String allowableDigits = "0123456789ABCDEF";
        String parseText = macAddressText.toUpperCase(Locale.US);
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < parseText.length(); i++)
        {
            if (allowableDigits.indexOf(parseText.charAt(i)) >=0)
            {
                builder.append(parseText.charAt(i));
            }
        }

        long value = Long.parseLong(builder.toString(),16);

        byte[] tempBytes = ByteBuffer.allocate(8).putLong(value).array();
        byte[] macBytes = Arrays.copyOfRange(tempBytes,2,8);
        return macBytes;
    }
}
