package com.wakeup.app.utility;


public class IntUtility {
    public static boolean isInteger( String input )
    {
        try
        {
            Integer.parseInt( input );
            return true;
        }
        catch( Exception ex )
        {
            return false;
        }
    }
}
