# WakeUp #

WakeUp is an Android application to send Wake On LAN packets to a device on the network, such as a server.  It can be used to ping a device too.

I created this basic application to:

* Turn on my home server
* Explore and learn about Android development, e.g. Activities and Fragments
* Learn Java syntax

Build
-----
Requires Android Studio